package com.fuxing.minapp.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.pagehelper.Page;

import java.util.List;

/**
 * 封装的标准响应格式
 * 200响应成功
 * 500服务器内部错误
 * 404未找到对应的资源
 * 502拦截token出错
 * 555异常抛出信息
 */
public class ResponseData {
    //响应状态
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String status;

    //响应消息
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    //响应数据
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<?> rows;

    private boolean success = true;

    //数据总数
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long total;

    public ResponseData() {
    }

    public ResponseData(boolean success) {
        setSuccess(success);
    }

    public ResponseData(List<?> list) {
        this(true);
        setRows(list);
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<?> getRows() {
        return rows;
    }

    public Long getTotal() {
        return total;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setRows(List<?> rows) {
        this.rows = rows;
        if (rows instanceof Page) {
            setTotal(((Page<?>) rows).getTotal());
        } else {
            setTotal((long) rows.size());
        }
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
