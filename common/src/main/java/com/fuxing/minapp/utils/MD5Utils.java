package com.fuxing.minapp.utils;

import org.apache.commons.codec.binary.Base64;

import java.security.MessageDigest;


public class MD5Utils {
    /**
     * 对字符串进行MD5加密
     */
    public static String getMD5Str(String strValue) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        String newStr = Base64.encodeBase64String(md.digest(strValue.getBytes()));
        return newStr;
    }
}
