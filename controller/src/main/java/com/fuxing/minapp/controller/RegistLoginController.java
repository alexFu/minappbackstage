package com.fuxing.minapp.controller;

import com.fuxing.minapp.pojo.Users;
import com.fuxing.minapp.service.IUserService;
import com.fuxing.minapp.utils.MD5Utils;
import com.fuxing.minapp.utils.ResponseData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "用户注册和登陆的接口",tags = {"用户注册和登陆的controller"})
public class RegistLoginController {

    @Autowired
    private IUserService service;

    //因为Users是一个json对象，所以需要加入@RequestBody
    @ApiOperation(value = "用户注册",notes = "用户注册的接口")
    @PostMapping("/regist")
    public ResponseData registMethod(@RequestBody Users users) throws Exception{
        ResponseData responseData = new ResponseData();
        //1.判断用户名是否不为空
        if(StringUtils.isBlank(users.getUsername()) || StringUtils.isBlank(users.getPassword())){
            responseData.setSuccess(false);
            responseData.setMessage("用户名或密码为空！");
            return responseData;
        }
        //2.判断用户名是否存在
        boolean isFlag = service.queryUserIsExist(users.getUsername());
        //3.保存用户，注册信息
        if(!isFlag){
            users.setNickname(users.getUsername());
            users.setPassword(MD5Utils.getMD5Str(users.getPassword()));
            users.setFansCounts(0);
            users.setFollowCounts(0);
            users.setReceiveLikeCounts(0);
            service.saveUser(users);
        }else{
            responseData.setSuccess(false);
            responseData.setMessage("用户名已存在，请换一个再试");
        }
        Users user = new Users();
        return responseData;
    }
}
