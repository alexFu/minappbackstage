package com.fuxing.minapp.mapper;

import com.fuxing.minapp.pojo.Bgm;
import com.fuxing.minapp.utils.MyMapper;

public interface BgmMapper extends MyMapper<Bgm> {
}