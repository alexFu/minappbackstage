package com.fuxing.minapp.mapper;

import java.util.List;

import com.fuxing.minapp.pojo.SearchRecords;
import com.fuxing.minapp.utils.MyMapper;

public interface SearchRecordsMapper extends MyMapper<SearchRecords> {
	
	public List<String> getHotwords();
}