package com.fuxing.minapp.mapper;

import com.fuxing.minapp.pojo.UsersLikeVideos;
import com.fuxing.minapp.utils.MyMapper;

public interface UsersLikeVideosMapper extends MyMapper<UsersLikeVideos> {
}