package com.fuxing.minapp.mapper;

import java.util.List;

import com.fuxing.minapp.pojo.Comments;
import com.fuxing.minapp.utils.MyMapper;

public interface CommentsMapperCustom extends MyMapper<Comments> {
	
	//public List<CommentsVO> queryComments(String videoId);
}