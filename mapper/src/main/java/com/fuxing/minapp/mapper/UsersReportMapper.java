package com.fuxing.minapp.mapper;

import com.fuxing.minapp.pojo.UsersReport;
import com.fuxing.minapp.utils.MyMapper;

public interface UsersReportMapper extends MyMapper<UsersReport> {
}