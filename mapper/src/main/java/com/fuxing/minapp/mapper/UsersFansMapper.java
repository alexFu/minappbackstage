package com.fuxing.minapp.mapper;

import com.fuxing.minapp.pojo.UsersFans;
import com.fuxing.minapp.utils.MyMapper;

public interface UsersFansMapper extends MyMapper<UsersFans> {
}