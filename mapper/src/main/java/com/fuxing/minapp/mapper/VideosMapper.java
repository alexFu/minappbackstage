package com.fuxing.minapp.mapper;

import com.fuxing.minapp.pojo.Videos;
import com.fuxing.minapp.utils.MyMapper;

public interface VideosMapper extends MyMapper<Videos> {
}