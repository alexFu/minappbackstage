package com.fuxing.minapp.mapper;

import com.fuxing.minapp.pojo.Comments;
import com.fuxing.minapp.utils.MyMapper;

public interface CommentsMapper extends MyMapper<Comments> {
}