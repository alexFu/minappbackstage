package com.fuxing.minapp.service.impl;

import com.fuxing.minapp.mapper.UsersMapper;
import com.fuxing.minapp.pojo.Users;
import com.fuxing.minapp.service.IUserService;

import org.n3r.idworker.Sid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UsersMapper mapper;

    @Autowired
    private Sid sid;

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public boolean queryUserIsExist(String userName) {
        Users users = new Users();
        users.setUsername(userName);
        Users userResult = mapper.selectOne(users);
        return userResult == null ? false : true ;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void saveUser(Users users) {
        String userId = sid.nextShort();
        users.setId(userId);
        mapper.insert(users);
    }
}
