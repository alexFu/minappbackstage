package com.fuxing.minapp.service;

import com.fuxing.minapp.pojo.Users;

public interface IUserService {
    /**
     * 判断用户名是否存在
     * @param userName 用户名
     * @return
     */
    public boolean queryUserIsExist(String userName);

    public void saveUser(Users users);
}
